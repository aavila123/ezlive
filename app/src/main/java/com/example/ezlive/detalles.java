package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class detalles extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalles);
        Log.e("Ciclo de vida", "onCreate");
        MostrarMensaje("onCreate");

    }

    private void MostrarMensaje(String mensaje){
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("Ciclo de vida", "onStart");
        MostrarMensaje("onStart");
    }
}
