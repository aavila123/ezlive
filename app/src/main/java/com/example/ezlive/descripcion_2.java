package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class descripcion_2 extends AppCompatActivity {

    ImageView flecha_2;
    ImageView sms_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_2);

        flecha_2 = (ImageView)findViewById(R.id.flecha_2);
        sms_2 = (ImageView)findViewById(R.id.sms_2);

        flecha_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regresar_1 = new Intent(descripcion_2.this, perfiles.class);
                startActivity(regresar_1);
            }
        });

        sms_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chat_1 = new Intent(descripcion_2.this, chat.class);
                startActivity(chat_1);
            }
        });
    }
}
