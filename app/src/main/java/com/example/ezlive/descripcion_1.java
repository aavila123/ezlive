package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.strictmode.IntentReceiverLeakedViolation;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class descripcion_1 extends AppCompatActivity {


    ImageView flecha;
    Button btn_solicitar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion_1);

        flecha = (ImageView)findViewById(R.id.flecha_1);
        btn_solicitar = (Button)findViewById(R.id.btn_solicitar);

        flecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regresar = new Intent(descripcion_1.this, perfiles.class);
                startActivity(regresar);
            }
        });


        btn_solicitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialogo = new Dialog(descripcion_1.this);
                dialogo.setContentView(R.layout.activity_solicitud);

                final TextView tv_des = (TextView)dialogo.findViewById(R.id.tv_des);
                final EditText txt_des = (EditText) dialogo.findViewById(R.id.txt_descripcion);
                final TextView tv_tiempo = (TextView) dialogo.findViewById(R.id.tv_tiempo);
                final EditText txt_tiempo = (EditText) dialogo.findViewById(R.id.txt_tiempo);
                final TextView tv_tarifa =(TextView) dialogo.findViewById(R.id.tv_tarifa);
                final EditText txt_tarifa = (EditText) dialogo.findViewById(R.id.txt_tarifa);
                final TextView tv_lugar = (TextView) dialogo.findViewById(R.id.tv_lugar);
                final EditText txt_lugar = (EditText) dialogo.findViewById(R.id.txt_lugar);
                Button btn_sol = (Button) dialogo.findViewById(R.id.btn_solicitar_dg);
                btn_sol.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(descripcion_1.this, "Solicitud de servicio enviada", Toast.LENGTH_SHORT).show();
                    }
                });
                dialogo.show();
            }
        });




    }
}
