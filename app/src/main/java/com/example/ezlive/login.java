package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.auth.FirebaseUser;

public class login extends AppCompatActivity {

    EditText txt_usuario;
    EditText txt_pwd;
    Button btn1;
    Button btn2;
    TextView txt_crear_cuenta;
    Typeface moonbright;
    String u;
    
    //FIREBASE 
    //private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        // Initialize Firebase Auth
        //mAuth = FirebaseAuth.getInstance();

        txt_usuario = (EditText)findViewById(R.id.txt_usuario);
        txt_pwd = (EditText)findViewById(R.id.txt_pwd);
        btn1 = (Button)findViewById(R.id.btn_ingresar);
        btn2 = (Button)findViewById(R.id.btn_crear);
        txt_crear_cuenta = (TextView) findViewById(R.id.txt_crear_cuenta);





        //FUENTE MOONBRIGHT
        String fuente_1 = "fuentes/dealer.ttf";
        this.moonbright = Typeface.createFromAsset(getAssets(),fuente_1);
        txt_crear_cuenta.setTypeface(moonbright);
        btn1.setTypeface(moonbright);
        btn2.setTypeface(moonbright);
        txt_usuario.setTypeface(moonbright);
        txt_pwd.setTypeface(moonbright);


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent boton_iniciar = new Intent(login.this, perfiles.class);
                startActivity(boton_iniciar);
                Toast.makeText(getApplicationContext(), "Hola " + txt_usuario.getText().toString(), Toast.LENGTH_LONG).show();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent boton_registrar = new Intent(login.this, registro.class);
                startActivity(boton_registrar);
            }
        });


    }

    //@Override
    //public void onStart() {
    //super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        //FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    //}

    //private void updateUI(FirebaseUser currentUser) {
    //}






}


