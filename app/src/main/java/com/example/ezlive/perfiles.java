package com.example.ezlive;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class perfiles extends AppCompatActivity {

    TextView perfiles;
    TextView name_1;
    TextView prof_1;
    TextView name_2;
    TextView prof_2;
    TextView name_u3;
    TextView prof_u3;
    ImageView u1;
    ImageView u2;
    ImageView u3;
    Button salir_perfiles;
    Typeface dealer;
    Typeface atraction;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfiles);

        u1 = (ImageView) findViewById(R.id.img_u1);
        u2 = (ImageView) findViewById(R.id.img_u2);
        perfiles = (TextView) findViewById(R.id.txt_perfiles_1);
        name_1 = (TextView) findViewById(R.id.nombre_u1);
        name_2 = (TextView) findViewById(R.id.nombre_u2);
        name_u3 = (TextView) findViewById(R.id.name_u3);
        prof_1 = (TextView) findViewById(R.id.profesion_u1);
        prof_2 = (TextView) findViewById(R.id.profesion_u2);
        prof_u3 = (TextView) findViewById(R.id.prof_u3);
        u3 = (ImageView)findViewById(R.id.img_u3);





        salir_perfiles = (Button)findViewById(R.id.btn_salir_perfiles);


        //FUENTE SEBASTIANA

        String fuente_atraction = "fuentes/atraction.ttf";
        this.atraction = Typeface.createFromAsset(getAssets(),fuente_atraction);
        perfiles.setTypeface(atraction);

        //FUENTE DEALER

        String fuente_dealer = "fuentes/dealer.ttf";
        this.dealer = Typeface.createFromAsset(getAssets(),fuente_dealer);
        salir_perfiles.setTypeface(dealer);
        name_1.setTypeface(dealer);
        name_2.setTypeface(dealer);
        prof_1.setTypeface(dealer);
        prof_2.setTypeface(dealer);
        name_u3.setTypeface(dealer);
        prof_u3.setTypeface(dealer);




        salir_perfiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent salir = new Intent(perfiles.this, login.class);
                startActivity(salir);
            }
        });

        u1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalles_1 = new Intent(perfiles.this, descripcion_1.class);
                startActivity(detalles_1);
            }
        });

        u2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalles_2 = new Intent(perfiles.this, descripcion_2.class);
                startActivity(detalles_2);
            }
        });

        u3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detalles_3 = new Intent(perfiles.this, descripcion_3.class);
                startActivity(detalles_3);
            }
        });
    }
}
