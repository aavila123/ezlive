package com.example.ezlive.utilidades;

public class utilidades {

    public static final String tabla_usuarios = "usuarios";
    public static final String campo_nombre = "nombre";
    public static final String campo_apellido = "apellido";
    public static final String campo_profesion = "profesion";
    public static final String campo_ciudad = "ciudad";
    public static final String campo_telefono = "telefono";
    public static final String campo_usuario = "usuario";
    public static final String campo_contraseña = "contraseña";

    public static final String crear_tabla = "CREATE TABLE " + tabla_usuarios + "(" + campo_nombre + " TEXT, " + campo_apellido + " TEXT, " + campo_profesion + " TEXT, " + campo_ciudad + " TEXT, " + campo_telefono + " INTEGER, " + campo_usuario + " TEXT, " + campo_contraseña + " TEXT)";

}
